<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('name_2');
            $table->string('col_3');
            $table->string('col_4');
            $table->string('col_5');
            $table->string('col_6');
            $table->string('col_7');
            $table->string('col_8');
            $table->string('col_9');
            $table->string('col_10');
            $table->string('col_11');
            $table->string('col_12');
            $table->string('col_13');
            $table->string('col_14');
            $table->string('col_15');
            $table->string('col_16');
            $table->string('col_17');
            $table->string('col_18');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
