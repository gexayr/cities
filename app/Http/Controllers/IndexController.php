<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cities;
use DB;

class IndexController extends Controller
{
    public function index()
    {


    $cities = Cities::paginate(10);


    $data = [
        'title'=>'Cities',
        'cities'=>$cities,
    ];

        return view('welcome',$data);

    }

    public function cronJob()
    {

        $filename = '../RU.csv';
        echo 'Размер файла ' . $filename . ': ' . filesize($filename) . ' байтов';

//        $str = '----cron-job----';
//        file_put_contents('history-cron.php', "\n"."----------------"."\n".$str."\n"."----------------"."\n", FILE_APPEND);

        $check = false;
        if(filesize($filename) != 52683887){
            $check = true;
        }
//
//        dd($check);
//        dd('cron');

        if($check == true){
            $csv = array_map('str_getcsv', file('../RU.csv'));

//            $cities = Cities::all();

            foreach ($csv as $item){
                $array = explode('	', $item[0]);

                $city = Cities::where('number', $array[0])->first();
                if($city != null){
                    if($city->name != $array[1] ||
                        $city->name_2 != $array[2] ||
                        $city->col_3 != $array[3] ||
                        $city->col_4 != $array[4] ||
                        $city->col_5 != $array[5] ||
                        $city->col_6 != $array[6] ||
                        $city->col_7 != $array[7] ||
                        $city->col_8 != $array[8] ||
                        $city->col_9 != $array[9] ||
                        $city->col_10 != $array[10] ||
                        $city->col_11 != $array[11] ||
                        $city->col_12 != $array[12] ||
                        $city->col_13 != $array[13] ||
                        $city->col_14 != $array[14] ||
                        $city->col_15 != $array[15] ||
                        $city->col_16 != $array[16] ||
                        $city->col_17 != $array[17] ||
                        $city->col_18 != $array[18]
                    ){
                        DB::table('cities')
                            ->where('number', $array[0])
                            ->update([
                                'name' => $array[1],
                                'name_2' => $array[2],
                                'col_3' => $array[3],
                                'col_4' => $array[4],
                                'col_5' => $array[5],
                                'col_6' => $array[6],
                                'col_7' => $array[7],
                                'col_8' => $array[8],
                                'col_9' => $array[9],
                                'col_10' => $array[10],
                                'col_11' => $array[11],
                                'col_12' => $array[12],
                                'col_13' => $array[13],
                                'col_14' => $array[14],
                                'col_15' => $array[15],
                                'col_16' => $array[16],
                                'col_17' => $array[17],
                                'col_18' => $array[18],
                            ]);
                    }
                 
                }else{

                    $cities = new Cities;
                    $cities->number = $array[0];
                    $cities->name = $array[1];
                    $cities->name_2 = $array[2];
                    $cities->col_3 = $array[3];
                    $cities->col_4 = $array[4];
                    $cities->col_5 = $array[5];
                    $cities->col_6 = $array[6];
                    $cities->col_7 = $array[7];
                    $cities->col_8 = $array[8];
                    $cities->col_9 = $array[9];
                    $cities->col_10 = $array[10];
                    $cities->col_11 = $array[11];
                    $cities->col_12 = $array[12];
                    $cities->col_13 = $array[13];
                    $cities->col_14 = $array[14];
                    $cities->col_15 = $array[15];
                    $cities->col_16 = $array[16];
                    $cities->col_17 = $array[17];
                    $cities->col_18 = $array[18];

                    $cities->save();

                }
            }
        }
    }

    public function autocomplete(Request $request)
    {

        $term = $request->term;

        if(strlen($term) >= 3){
            $items = Cities::where('name', 'LIKE', $term.'%')->get();
            if(count($items) == 0){
                $searchResult[] = 'Not Found';
            }else{
                foreach ($items as $key=>$value){
                    $searchResult[] = $value->name.'_#'.$value->number;
                }
            }

            return $searchResult;

        }else{
            $searchResult[] = "left ".(3-strlen($term)). ' letters';
            return $searchResult;
        }

    }

    public function search(Request $request)
    {
        $name = $request->searchItem;

        if (strpos($name, '_#') !== false) {
            $array_name = explode('_#',$name);
            $name = $array_name[0];
            $number = $array_name[1];
            $cities = Cities::where('name', $name)
                ->where('number', $number)
                ->first();
        }else{
            $cities = Cities::where('name', $name)->first();
        }

        $col_4 = $cities->col_4;
        $col_5 = $cities->col_5;
        $near_cities = $this->getNearCities(array(),$col_4, $col_5);

        foreach($near_cities as $key=>$value){
            if($cities->toArray() == $value){
                unset($near_cities[$key]);
            }
        }

        $data = [
            'title'=>'Cities',
            'first_city'=>$cities,
            'cities'=>$near_cities,
    ];

        return view('cities',$data);

    }

    public function getNearCities($arr_near_cities,$lat,$lng)
    {

        $near_cities = Cities::where('col_4', 'LIKE', $lat.'%')
            ->where('col_5', 'LIKE', $lng.'%')
            ->where('col_7', '!=', 'LK')
            ->limit(50)
            ->get();
        $near_cities = $near_cities->toArray();
        foreach ($near_cities as $item){
            if (!in_array($item, $arr_near_cities)) {
                $arr_near_cities[] = $item;
            }

            if(count($arr_near_cities)==20){
                return $arr_near_cities;
            }
        }
//===========
        $length_lat = strlen($lat);
        $rest = substr($lat, $length_lat-2, 2);
        if (strpos($rest, '.') !== false) {
            $new_rest = ($rest- 0.5);
        }else{
            $new_rest = ($rest - 5);
        }
        $new_lat = substr($lat, 0, $length_lat-2);
        $new_lat = $new_lat.$new_rest;
//============
        $length_lng = strlen($lng);
        $rest = substr($lng, $length_lng-2, 2);
        if (strpos($rest, '.') !== false) {
            $new_rest = ($rest- 0.5);
        }else{
            $new_rest = ($rest - 5);
        }
        $new_lng = substr($lng, 0, $length_lng-2);
        $new_lng = $new_lng.$new_rest;
//===========
        $new_lat = substr($new_lat,0, strlen($new_lat)-1);
        $new_lng = substr($new_lng,0, strlen($new_lng)-1);
        $near_cities = Cities::where('col_4', 'LIKE', $new_lat.'%')
            ->where('col_5', 'LIKE', $new_lng.'%')
            ->where('col_7', '!=', 'LK')
            ->limit(50)
            ->get();
        $near_cities = $near_cities->toArray();
        foreach ($near_cities as $item){
            if (!in_array($item, $arr_near_cities)) {
                $arr_near_cities[] = $item;
            }

            if(count($arr_near_cities)==20){
                return $arr_near_cities;
            }
        }

//===========
        $length_lat = strlen($lat);
        $rest = substr($lat, $length_lat-2, 2);
        if (strpos($rest, '.') !== false) {
            $rest = substr($lat, $length_lat-3, 3);
            $new_rest = ($rest + 0.5);
        }else{
            $new_rest = ($rest + 5);
        }
        $new_lat = substr($lat, 0, $length_lat-2);
        $new_lat = $new_lat.$new_rest;
//============
        $length_lng = strlen($lng);
        $rest = substr($lng, $length_lng-2, 2);
        if (strpos($rest, '.') !== false) {
            $rest = substr($lat, $length_lat-3, 3);
            $new_rest = ($rest + 0.5);
        }else{
            $new_rest = ($rest + 5);
        }
        $new_lng = substr($lng, 0, $length_lng-2);
        $new_lng = $new_lng.$new_rest;
//===========

        $col_4 = substr($new_lat,0, strlen($new_lat)-1);
        $col_3 = substr($new_lng,0, strlen($new_lng)-1);
        return $this->getNearCities($arr_near_cities, $col_4, $col_3);

    }
}
