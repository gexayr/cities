<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    <title>{{ $title }}</title>
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <nav class="navbar navbar-light bg-light">
            <form class="form-inline" action="search" method="post">
                {{ csrf_field() }}
                <input id="searchItem" name="searchItem" class="form-control mr-sm-2" type="text" placeholder="Search" value="{{$first_city->name}}">
                <button class="btn btn-outline-success my-3 my-sm-20" type="submit">Search</button>
            </form>
        </nav>
        <div class="container">
            <div id="map"></div>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Coordinate Latitude(lat)</th>
                    <th>Coordinate Longitude(lng)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>{{ $first_city->id }}</th>
                    <th>{{ $first_city->number }}</th>
                    <th>{{ $first_city->name }}</th>
                    <th>{{ $first_city->col_3 }}</th>
                    <th>{{ $first_city->col_4 }}</th>
                    <th>{{ $first_city->col_5 }}</th>
                </tr>
                @foreach($cities as $city)
                    <tr>
                        <td>{{ $city['id'] }}</td>
                        <td>{{ $city['number'] }}</td>
                        <td>{{ $city['name'] }}</td>
                        <td>{{ $city['col_3'] }}</td>
                        <td>{{ $city['col_4'] }}</td>
                        <td>{{ $city['col_5'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script
        src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<script>
    function initMap() {
        var lat = {{$first_city->col_4}};
        var lng = {{$first_city->col_5}};
        var content = "{!! $first_city->col_3 !!}";


        console.log(content);
        var uluru = {lat: lat, lng: lng};
        var element = document.getElementById('map');
        var options = {
            zoom: 11,
            center: uluru,

        }
        var map = new google.maps.Map(element, options);

        var markers = [
            {
                coordinates: {lat: 	lat, lng: lng},
                info: content,
                image: '/map-marker.png'
            },
<?php
    foreach ($cities as $city){
        if($city['id'] != $first_city->id){
            $col_4 = $city['col_4'];
            $col_5 = $city['col_5'];
            $col_3 = $city['col_3'];
        echo "    {
                coordinates: {lat: $col_4, lng: $col_5},
                image: '/marker.png',
                info: \"$col_3\",
            },";
        }

    }
?>
        ];

        for(var i = 0; i < markers.length; i++){
            addMarker(markers[i]);
        }

        function addMarker(properties) {
            var marker = new google.maps.Marker({
                position: properties.coordinates,
                map: map,
                icon: properties.image,
            });


            if(properties.image){
                marker.setIcon(properties.image)
            }
            if(properties.info){
                var InfoWindow = new google.maps.InfoWindow({
                    content: properties.info
                });

                marker.addListener('click',function () {
                    InfoWindow.open(map,marker);
                })
            }
        }



    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTb6YsX1CDm6L-ZFHCYzZel9Xw7RL06Dg&callback=initMap">
</script>
<script src="{{asset('/js/script.js')}}"></script>
</body>
</html>