<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    <title>{{ $title }}</title>
</head>
<body>

<div class="container">
    <div class="row">
        <nav class="navbar navbar-light bg-light">
            <form class="form-inline" action="search" method="post">
                {{ csrf_field() }}
                <input id="searchItem" name="searchItem" class="form-control mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-outline-success my-3 my-sm-20" type="submit">Search</button>
            </form>
        </nav>
        <div class="container">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Coordinate Latitude</th>
                    <th>Coordinate Longitude</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cities as $city)
                    <tr>
                        <td>{{ $city->id }}</td>
                        <td>{{ $city->number }}</td>
                        <td>{{ $city->name }}</td>
                        <td>{{ $city->col_4 }}</td>
                        <td>{{ $city->col_5 }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-center center pagination">
                {{ $cities->links() }}
            </div>
        </div>
    </div>
</div>
<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function () {

    });

</script>
<script src="{{asset('/js/script.js')}}"></script>
</body>
</html>